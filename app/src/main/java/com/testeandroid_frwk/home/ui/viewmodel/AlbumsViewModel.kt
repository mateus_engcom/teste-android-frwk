package com.testeandroid_frwk.home.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.testeandroid_frwk.base.Application
import com.testeandroid_frwk.home.data.TypicodeApiService
import com.testeandroid_frwk.home.data.model.Album
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AlbumsViewModel : ViewModel() {
    val albumList: MutableLiveData<List<Album>> by lazy {
        MutableLiveData<List<Album>>()
    }

    init {
        val list = Application.database?.DAOAlbum()?.getAlbums()
        if (list.isNullOrEmpty())
            getAlbumData()
        else
            albumList.value = list
    }

    fun getAlbumData() {
        val repository = TypicodeApiService.create()

        repository.getUserList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ users ->

                repository.getAlbums()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .flatMap {
                        Observable.fromIterable(it)
                    }
                    .flatMap { album ->
                        album.userName = (users.find { it.id == album.userId })?.username
                        Observable.just(album)
                    }
                    .toList()
                    .subscribe({ result ->
                        albumList.value = result
                        Application.database?.DAOAlbum()?.saveAlbums(result)
                    }, { error ->
                        error.printStackTrace()
                    })

            }, { error ->
                error.printStackTrace()
            })
    }
}
