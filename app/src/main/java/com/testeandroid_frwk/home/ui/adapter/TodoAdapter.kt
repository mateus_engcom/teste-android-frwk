package com.testeandroid_frwk.home.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.testeandroid_frwk.R
import com.testeandroid_frwk.base.Application
import com.testeandroid_frwk.home.data.model.Todo
import kotlinx.android.synthetic.main.item_todo.view.*


class TodoAdapter(
    private val list: List<Todo>,
    private val context: Context
) : RecyclerView.Adapter<TodoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_todo, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cbItem.text = list[position].title
        holder.cbItem.isChecked = list[position].completed
        holder.cbItem.setOnCheckedChangeListener { buttonView, isChecked ->
            val item = list[position]
            item.completed = isChecked
            Application.database?.DAOTodo()?.updateItem(item)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cbItem: CheckBox = itemView.cbItem
    }
}