package com.testeandroid_frwk.home.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.testeandroid_frwk.base.Application
import com.testeandroid_frwk.home.data.TypicodeApiService
import com.testeandroid_frwk.home.data.model.Todo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TodoViewModel : ViewModel() {
    val todoList: MutableLiveData<List<Todo>> by lazy {
        MutableLiveData<List<Todo>>()
    }

    init {
        val list = Application.database?.DAOTodo()?.getTodos()
        if (list.isNullOrEmpty())
            getTodoData()
        else
            todoList.value = list
    }

    fun getTodoData() {
        val repository = TypicodeApiService.create()
        repository.getTodoList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ result ->
                todoList.value = result
                Application.database?.DAOTodo()?.saveTodos(result)

            }, { error ->
                error.printStackTrace()
            })
    }
}
