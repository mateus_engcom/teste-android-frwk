package com.testeandroid_frwk.home.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation.findNavController
import com.testeandroid_frwk.R
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            if (item.itemId == R.id.navigation_albums) {
                findNavController(navHostFragment.view!!).navigate(R.id.albumsFragment)
            } else if (item.itemId == R.id.navigation_posts) {
                findNavController(navHostFragment.view!!).navigate(R.id.postsFragment)
            } else if (item.itemId == R.id.navigation_todo) {
                findNavController(navHostFragment.view!!).navigate(R.id.todoFragment)
            }
            true
        }
    }
}
