package com.testeandroid_frwk.home.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.testeandroid_frwk.R
import com.testeandroid_frwk.home.data.model.Post
import com.testeandroid_frwk.home.ui.adapter.PostAdapter
import com.testeandroid_frwk.home.ui.viewmodel.PostsViewModel
import kotlinx.android.synthetic.main.posts_fragment.*

class PostsFragment : Fragment() {

    companion object {
        fun newInstance() = PostsFragment()
    }

    private lateinit var viewModel: PostsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.posts_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PostsViewModel::class.java)

        val dataObserver = Observer<List<Post>> { list ->
            rcvPosts.apply {
                context?.let {
                    adapter = PostAdapter(list, it)
                    layoutManager = LinearLayoutManager(it)
                }
            }
        }

        viewModel.postList.observe(this, dataObserver)
    }
}
