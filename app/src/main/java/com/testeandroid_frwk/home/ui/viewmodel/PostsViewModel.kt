package com.testeandroid_frwk.home.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.testeandroid_frwk.base.Application
import com.testeandroid_frwk.home.data.TypicodeApiService
import com.testeandroid_frwk.home.data.model.Post
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PostsViewModel : ViewModel() {

    val postList: MutableLiveData<List<Post>> by lazy {
        MutableLiveData<List<Post>>()
    }

    init {
        val list = Application.database?.DAOPosts()?.getPosts()
        if (list.isNullOrEmpty())
            getPostData()
        else
            postList.value = list
    }

    fun getPostData() {
        val repository = TypicodeApiService.create()

        repository.getUserList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ users ->

                repository.getPosts()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .flatMap {
                        Observable.fromIterable(it)
                    }
                    .flatMap { post ->
                        post.userName = (users.find { it.id == post.userId })?.username
                        Observable.just(post)
                    }
                    .toList()
                    .subscribe({ result ->
                        postList.value = result
                        Application.database?.DAOPosts()?.savePosts(result)
                    }, { error ->
                        error.printStackTrace()
                    })

            }, { error ->
                error.printStackTrace()
            })
    }
}
