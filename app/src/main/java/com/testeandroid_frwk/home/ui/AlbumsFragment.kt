package com.testeandroid_frwk.home.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.testeandroid_frwk.R
import com.testeandroid_frwk.home.data.model.Album
import com.testeandroid_frwk.home.ui.adapter.AlbumAdapter
import com.testeandroid_frwk.home.ui.viewmodel.AlbumsViewModel
import kotlinx.android.synthetic.main.albums_fragment.*

class AlbumsFragment : Fragment() {

    companion object {
        fun newInstance() = AlbumsFragment()
    }

    private lateinit var viewModel: AlbumsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.albums_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AlbumsViewModel::class.java)

        val dataObserver = Observer<List<Album>> { list ->
            rcvAlbums.apply {
                context?.let {
                    adapter = AlbumAdapter(list, it)
                    layoutManager = GridLayoutManager(it, 2)
                }
            }
        }

        viewModel.albumList.observe(this, dataObserver)
    }
}
