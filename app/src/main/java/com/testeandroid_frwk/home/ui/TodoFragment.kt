package com.testeandroid_frwk.home.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.testeandroid_frwk.R
import com.testeandroid_frwk.home.data.model.Todo
import com.testeandroid_frwk.home.ui.adapter.TodoAdapter
import com.testeandroid_frwk.home.ui.viewmodel.TodoViewModel
import kotlinx.android.synthetic.main.todo_fragment.*

class TodoFragment : Fragment() {

    companion object {
        fun newInstance() = TodoFragment()
    }

    private lateinit var viewModel: TodoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.todo_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TodoViewModel::class.java)

        val dataObserver = Observer<List<Todo>> { list ->
            rcvTodo.apply {
                context?.let {
                    adapter = TodoAdapter(list, it)
                    layoutManager = LinearLayoutManager(it)
                }
            }
        }

        viewModel.todoList.observe(this, dataObserver)
    }

}
