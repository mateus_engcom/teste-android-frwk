package com.testeandroid_frwk.home.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.abdularis.civ.AvatarImageView
import com.testeandroid_frwk.R
import com.testeandroid_frwk.base.gone
import com.testeandroid_frwk.home.data.model.Album
import kotlinx.android.synthetic.main.item_album.view.*


class AlbumAdapter(
    private val list: List<Album>,
    private val context: Context
) : RecyclerView.Adapter<AlbumAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_album, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = list[position].title
        list[position].userName?.let {
            holder.autorName.text = it
            holder.avatar.setText(it)
        } ?: holder.avatar.gone()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.txtPostTitle
        val autorName: TextView = itemView.txtAutorName
        val avatar: AvatarImageView = itemView.avatarImageView
    }
}