package com.testeandroid_frwk.home.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "post")
class Post(
    @SerializedName("userId") val userId: Long,
    @SerializedName("id") @PrimaryKey val id: Long,
    @SerializedName("title") val title: String,
    @SerializedName("body") val body: String,
    var userName: String?
)