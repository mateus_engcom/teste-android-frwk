package com.testeandroid_frwk.home.data

import androidx.room.*
import com.testeandroid_frwk.home.data.model.Album
import com.testeandroid_frwk.home.data.model.Post
import com.testeandroid_frwk.home.data.model.Todo

@Dao
interface DAOPosts {
    @Query("SELECT * FROM post")
    fun getPosts(): List<Post>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun savePosts(posts: List<Post>)
}

@Dao
interface DAOAlbum {
    @Query("SELECT * FROM album")
    fun getAlbums(): List<Album>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAlbums(albums: List<Album>)
}

@Dao
interface DAOTodo {
    @Query("SELECT * FROM todo")
    fun getTodos(): List<Todo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveTodos(todos: List<Todo>)

    @Update
    fun updateItem(itemTodo: Todo)
}