package com.testeandroid_frwk.home.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.testeandroid_frwk.home.data.model.Album
import com.testeandroid_frwk.home.data.model.Post
import com.testeandroid_frwk.home.data.model.Todo


@Database(version = 1, entities = arrayOf(Post::class, Album::class, Todo::class))
abstract class AppDataBase : RoomDatabase() {
    abstract fun DAOPosts(): DAOPosts
    abstract fun DAOAlbum(): DAOAlbum
    abstract fun DAOTodo(): DAOTodo
}