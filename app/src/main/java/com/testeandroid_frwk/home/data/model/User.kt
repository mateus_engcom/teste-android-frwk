package com.testeandroid_frwk.home.data.model

import com.google.gson.annotations.SerializedName

class User(
    @SerializedName("id") val id: Long,
    @SerializedName("username") val username: String
)