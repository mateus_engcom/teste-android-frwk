package com.testeandroid_frwk.home.data

import com.testeandroid_frwk.home.data.model.Album
import com.testeandroid_frwk.home.data.model.Post
import com.testeandroid_frwk.home.data.model.Todo
import com.testeandroid_frwk.home.data.model.User
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface TypicodeApiService {

    @GET("posts")
    fun getPosts(): Observable<List<Post>>

    @GET("albums")
    fun getAlbums(): Observable<List<Album>>

    @GET("todos")
    fun getTodoList(): Observable<List<Todo>>

    @GET("users")
    fun getUserList(): Observable<List<User>>

    companion object Factory {
        fun create(): TypicodeApiService {
            val client = OkHttpClient()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .build()

            return retrofit.create(TypicodeApiService::class.java)
        }
    }
}