package com.testeandroid_frwk.base

import android.app.Application
import androidx.room.Room
import com.testeandroid_frwk.home.data.AppDataBase

class Application : Application() {

    companion object {
        var database: AppDataBase? = null
    }

    override fun onCreate() {
        super.onCreate()
        //Room
        database = Room.databaseBuilder(this, AppDataBase::class.java, "my-db").allowMainThreadQueries().build()
    }
}