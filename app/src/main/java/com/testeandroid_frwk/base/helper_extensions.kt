package com.testeandroid_frwk.base

import android.view.View
import android.view.View.GONE

fun View.gone() {
    this.visibility = GONE
}